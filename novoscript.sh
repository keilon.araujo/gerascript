#!/bin/bash
#
# novoscript.sh - cria um novo script, já com atribuição de execução. 
#                                                                     
# Site      : http://gitlab.com/keilon.araujo                         
# Autor	    : Keilon Araujo <keilon.araujo@gmail.com                  
# Manutenção: Keilon Araujo <keilon.araujo@gmail.com                  
#
# --------------------------------------------------------------------
#
# Este programa recebe uma string como parâmetro, a partir deste cria
# um novo arquivo, dando a ele permissão de execução e também 
# já abrindo para edição no editor Vim no STDOUT.
#
# Exemplos:
# 	$./novoscript.sh changeip.sh
#	
# Será criado o arquivo 'changeip.sh' com permissão de execução.
# Caso o usuário não digite nenhum caractere ou digite o nome de um 
# arquivo já existente, serão exibidas mensagens de erro.
#
# Exemplos:
#	:~/.scripts$ ./novoscript.sh novoscript.sh 
#	Arquivo já existe! Saindo...
#	:~/.scripts$ ./novoscript.sh 
#	Digite o nome de apenas um arquivo! Saindo...
#
# -------------------------------------------------------------------
#
# Histórico:
#
#	v1.0 2020-05-11
#	 - Esta é a primeira versão e está em fase de testes.
#
# Licença: MIT


# 'test' faz a verificação do valor digitado
[[ $# -ne 1 ]] && echo "Digite o nome de apenas um arquivo! Saindo..." &&
exit 1

# verificando a existentcia do arquivo no diretório current
[[ -f $1 ]] && echo "Arquivo já existe! Saindo..." && exit 1

# Este script será executado no ambiente bash
echo '#!/bin/bash
#
# novoscript.sh - cria um novo script, já com atribuição de execução.
#
# Site      : http://gitlab.com/keilon.araujo
# Autor	    : Keilon Araujo <keilon.araujo@gmail.com
# Manutenção: Keilon Araujo <keilon.araujo@gmail.com
#
# --------------------------------------------------------------------
#
# Descrição :
#
# Exemplos:
# 	$./novoscript.sh changeip.sh
#
#
# -------------------------------------------------------------------
#
# Histórico:
#
#	v1.0 2020-05-11
#
# Licença: MIT

' >> $1

# dando permissão de execução ao arquivo
chmod +x $1

# abrindo o arquivo criado no editor Vim
vim $1
